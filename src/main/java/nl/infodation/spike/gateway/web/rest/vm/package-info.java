/**
 * View Models used by Spring MVC REST controllers.
 */
package nl.infodation.spike.gateway.web.rest.vm;
